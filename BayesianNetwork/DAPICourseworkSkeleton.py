#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Coursework in Python
from DAPICourseworkLibrary import *
from numpy import *
#
# Coursework 1 begins here
#
# Function to compute the prior distribution of the variable root from the data set
def Prior(theData, root, noStates):
    prior = zeros((noStates[root]), float )
# Coursework 1 task 1 should be inserted here
    count = []
    for i in range(0, noStates[root], 1):
        count.append(0)
    count = array(count)
    length = len(theData)
    noVariables = len(noStates)
    for i in range(0, length, 1):
        count[theData[i][root]] += 1
    for i in range(0, noStates[root], 1):
        prior[i] = count[i] / float(length)
# end of Coursework 1 task 1
    return prior
# Function to compute a CPT with parent node varP and xchild node varC from the data array
# it is assumed that the states are designated by consecutive integers starting with 0
def CPT(theData, varC, varP, noStates):
    cPT = zeros((noStates[varC], noStates[varP]), float )
# Coursework 1 task 2 should be inserte4d here
    count = zeros((noStates[varC], noStates[varP]), int)
    length = len(theData)
    for i in range(0, length, 1):
        count [theData[i][varC]][theData[i][varP]] += 1
    for i in range(0, noStates[varP], 1):
        s = 0
        for j in range(0, noStates[varC], 1):
            s += count[j][i]
        for j in range(0, noStates[varC], 1):
            cPT[j][i] = count[j][i] / float (s)
# end of coursework 1 task 2
    return cPT
# Function to calculate the joint probability table of two variables in the data set
def JPT(theData, varRow, varCol, noStates):
    jPT = zeros((noStates[varRow], noStates[varCol]), float )
#Coursework 1 task 3 should be inserted here
    count = zeros((noStates[varRow], noStates[varCol]), int)
    data_size = len(theData)
    for i in range(0, data_size, 1):
        count [theData[i][varRow]][theData[i][varCol]] += 1
    for i in range(0, noStates[varRow], 1):
        for j in range(0, noStates[varCol], 1):
            jPT[i][j] = count[i][j] / float (data_size)
# end of coursework 1 task 3
    return jPT
#
# Function to convert a joint probability table to a conditional probability table
def JPT2CPT(aJPT):
#Coursework 1 task 4 should be inserted here
    number_of_row = len(aJPT)
    number_of_column = len(aJPT[0])
    prior = zeros(number_of_column, float )
    for i in range(0, number_of_column, 1):
        for j in range(0, number_of_row, 1):
            prior[i] += aJPT[j][i]
    for i in range(0, number_of_column, 1):
        for j in range(0, number_of_row, 1):
            aJPT[j][i] /= prior[i]
# coursework 1 taks 4 ends here
    return aJPT

#
# Function to query a naive Bayesian network
def Query(theQuery, naiveBayes):
    rootPdf = zeros((naiveBayes[0].shape[0]), float)
# Coursework 1 task 5 should be inserted here
    for i in range(0, len(rootPdf), 1):
        rootPdf[i] = 1
        for j in range(0, len(theQuery), 1):
            print (naiveBayes[j+1][theQuery[j]][i])
            rootPdf[i] *= naiveBayes[j+1][theQuery[j]][i]
    for i in range(0, len(rootPdf), 1):
        rootPdf[i] *= naiveBayes[0][i]
    s = sum(rootPdf)
    for i in range(0, len(rootPdf), 1):
        rootPdf[i] /= s
# end of coursework 1 task 5
    return rootPdf
#
# End of Coursework 1
#
#
# Coursework 2 begins here
#
# Calculate the mutual information from the joint probability table of two variables
def MutualInformation(jP):
    mi=0.0
# Coursework 2 task 1 should be inserted here


# end of coursework 2 task 1
    return mi
#
# construct a dependency matrix for all the variables
def DependencyMatrix(theData, noVariables, noStates):
    MIMatrix = zeros((noVariables,noVariables))
# Coursework 2 task 2 should be inserted here


# end of coursework 2 task 2
    return MIMatrix
# Function to compute an ordered list of dependencies
def DependencyList(depMatrix):
    depList=[]
# Coursework 2 task 3 should be inserted here


# end of coursework 2 task 3
    return array(depList2)
#
# Functions implementing the spanning tree algorithm
# Coursework 2 task 4

def SpanningTreeAlgorithm(depList, noVariables):
    spanningTree = []

    return array(spanningTree)
#
# End of coursework 2
#

#
# main program part for Coursework 1
#
noVariables, noRoots, noStates, noDataPoints, datain = ReadFile("Neurones.txt")
theData = array(datain)
AppendString("results.txt","Coursework One Results by Fei Gao")
AppendString("results.txt","Login: fg1417")
AppendString("results.txt","CID: 01394340")
AppendString("results.txt","") #blank line
AppendString("results.txt","The prior probability of node 0")
prior = Prior(theData, 0, noStates)
AppendList("results.txt", prior)
#
# continue as described
AppendString("results.txt","The conditional probability matrix P(2|0) calculated from data")
cpt = CPT(theData, 2, 0, noStates)
AppendArray("results.txt", cpt)
AppendString("results.txt","The joint probability matrix P(2&0)")
jpt = JPT(theData, 2, 0, noStates)
AppendArray("results.txt", jpt)
AppendString("results.txt","The conditional probability matrix P(2|0) calculated from the joint probability P(2&0)")
ajpt = JPT2CPT(jpt)
AppendArray("results.txt", ajpt)
AppendString("results.txt","The results of queries on the naive network [4, 0, 0, 0, 5]")
cpt1 = CPT(theData, 1, 0, noStates)
cpt2 = CPT(theData, 2, 0, noStates)
cpt3 = CPT(theData, 3, 0, noStates)
cpt4 = CPT(theData, 4, 0, noStates)
cpt5 = CPT(theData, 5, 0, noStates)
posterior1 = Query([4, 0, 0, 0, 5], [prior, cpt1, cpt2, cpt3, cpt4, cpt5])
AppendList("results.txt", posterior1)
AppendString("results.txt","The results of queries on the naive network [6, 5, 2, 5, 5]")
posterior2 = Query([6, 5, 2, 5, 5], [prior, cpt1, cpt2, cpt3, cpt4, cpt5])
AppendList("results.txt", posterior2)
